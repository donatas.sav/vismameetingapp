using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Visma_meetings;
using Visma_meetings.Models;

namespace Visma_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            MeetingApp app1 = new MeetingApp("db.json");
            bool result = app1.MeetingsInited();
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestCreateMeeting()
        {
            MeetingApp app1 = new MeetingApp("db.json");
            int meetingsBefore = app1.NumberOfMeetings();

            //
            app1.AutomaticValues.Add("Test Meeting");
            app1.AutomaticValues.Add("Donatas");
            app1.AutomaticValues.Add("1");
            app1.AutomaticValues.Add("2");
            app1.AutomaticValues.Add("2021-01-02");
            app1.AutomaticValues.Add("2021-02-01");
            app1.AutomaticValues.Add("name1");
            app1.AutomaticValues.Add("name2");
            app1.AutomaticValues.Add("");
            //

            app1.CreateMeeting();

            int meetingsAfter = app1.NumberOfMeetings();

            Assert.AreEqual(meetingsBefore + 1, meetingsAfter);
        }

        [TestMethod]
        public void TestBulkCreateMeetings()
        {
            MeetingApp app1 = new MeetingApp("F:/db.json");
            int meetingsBefore = app1.NumberOfMeetings();

            int AMOUNTOFNEWMEETINGS = 500;
            for (int i = 0; i < AMOUNTOFNEWMEETINGS; i++)
            {
                GenerateRandomMeetingData(app1);
                app1.CreateMeeting();
            }

            int meetingsAfter = app1.NumberOfMeetings();

            Assert.AreEqual(meetingsBefore + AMOUNTOFNEWMEETINGS, meetingsAfter);
        }

        public void GenerateRandomMeetingData(MeetingApp app1)
        {
            app1.AutomaticValues.Add(Guid.NewGuid().ToString());
            app1.AutomaticValues.Add(Guid.NewGuid().ToString());
            app1.AutomaticValues.Add("1");
            app1.AutomaticValues.Add("2");
            app1.AutomaticValues.Add("2021-01-02");
            app1.AutomaticValues.Add("2021-02-01");
            app1.AutomaticValues.Add(Guid.NewGuid().ToString());
            app1.AutomaticValues.Add(Guid.NewGuid().ToString());
            app1.AutomaticValues.Add("");
        }

        [TestMethod]
        public void TestIfCreatedExists()
        {
            MeetingApp app1 = new MeetingApp("db.json");

            string UniqueDescription = Guid.NewGuid().ToString();
            //
            app1.AutomaticValues.Add(UniqueDescription);
            app1.AutomaticValues.Add("Donatas");
            app1.AutomaticValues.Add("1");
            app1.AutomaticValues.Add("2");
            app1.AutomaticValues.Add("2021-01-02");
            app1.AutomaticValues.Add("2021-02-01");
            app1.AutomaticValues.Add("name1");
            app1.AutomaticValues.Add("name2");
            app1.AutomaticValues.Add("");
            app1.CreateMeeting();
            //

            // inputs used to navigate filtering menus
            app1.AutomaticValues.Add("0");
            app1.AutomaticValues.Add(UniqueDescription);
            Assert.IsTrue(InMeetingsList(app1.FilterMeetings(), UniqueDescription));
            // inputs used to navigate filtering menus
        }

        public bool InMeetingsList(Meeting[] list, string UniqueDescription)
        {
            foreach (var meeting in list)
            {
                if (meeting.Description == UniqueDescription)
                    return true;
            }
            return false;
        }
    }
}