﻿using Newtonsoft.Json;
using Visma_meetings.Models;

namespace Visma_meetings
{
    internal class Database
    {
        private string databasePath;
        public Database(string dbPath)
        {
            databasePath = dbPath;
        }

        public Meeting[] ReadDatabase()
        {
            InitDb();
            return JsonConvert.DeserializeObject<Meeting[]>(File.ReadAllText(databasePath));
        }

        private void InitDb()
        {
            if (!File.Exists(databasePath))
            {
                File.WriteAllText(databasePath, "[]");
                Console.WriteLine("Db file `{0}` not found, initializing empty db file in specified path", databasePath);
            }
        }

        public void SaveToDb(Meeting[] meetings)
        {
            if (meetings != null)
            {
                File.WriteAllText(databasePath, JsonConvert.SerializeObject(meetings, Formatting.Indented));
                Console.WriteLine("Db file `{0}` was saved", databasePath);
            }
        }
    }
}
