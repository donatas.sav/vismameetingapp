﻿
namespace Visma_meetings.Models
{
    public enum Category
    {
        CodeMonkey = 1,
        Hub = 2,
        Short = 3,
        TeamBuilding = 4
    }

    public enum Type
    {
        Live = 1,
        InPerson = 2
    }

    public enum MainNavigation
    {
        CreateMeeting = 1,
        DeleteMeeting = 2,
        AddPersonToMeeting = 3,
        RemovePersonFromMeeting = 4,
        FilterMeetings = 5,
        Exit = 9,
    }

    public enum FilterNavigation
    {
        ByDescription,
        ByResponsiblePerson,
        ByCategory,
        ByType,
        ByDate,
        ByNumberOfAttendees
    }
    public class Meeting
    {
        public Guid Id { get; set; }
        public string[] Name { get; set; }
        public string ResponsiblePerson { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }
        public Type Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
