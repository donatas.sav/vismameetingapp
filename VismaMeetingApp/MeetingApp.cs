﻿using Visma_meetings.Models;

namespace Visma_meetings
{
    public class MeetingApp
    {
        public Meeting[] meetings;     //global variables(klase)
        public List<string> AutomaticValues = new List<string>();

        private Database db;
        private string MeetingNotFoundText = "No scheduled meetings found under specified search criteria.";
        public MeetingApp(string databasePath)
        {
            db = new Database(databasePath);
            meetings = db.ReadDatabase();
            Console.WriteLine("Read database file {0} entries found", meetings.Length);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CloseProgramHandler); // hooking into Exit process to execute custom function
        }

        void CloseProgramHandler(object sender, EventArgs e)
        {
            db.SaveToDb(meetings);
        }

        public bool MeetingsInited()
        {
            return (meetings != null);
        }

        public int NumberOfMeetings()
        {
            return (meetings.Length);
        }

        public void Start()
        {
            string navigationValues = GetMainNavigation();
            Console.WriteLine("Welcome to Visma internal meetings application.");   //1-st console line

            while (true)
            {
                bool stopApp = false;
                MainNavigation option = (MainNavigation)GetNumberInput(string.Format("Navigation {0}\n" + "Select action: ", navigationValues));
                switch (option)
                {
                    // Main tasks.
                    case MainNavigation.CreateMeeting:
                        CreateMeeting();
                        break;
                    case MainNavigation.DeleteMeeting:
                        DeleteMeeting();
                        break;
                    case MainNavigation.AddPersonToMeeting:
                        AddPersonToMeeting();
                        break;
                    case MainNavigation.RemovePersonFromMeeting:
                        RemovePersonFromMeeting();
                        break;
                    case MainNavigation.FilterMeetings:
                        FilterMeetings();
                        break;
                    case MainNavigation.Exit:
                        stopApp = true;
                        break;
                    default:
                        break;
                }
                if (stopApp)
                    break;
            }
        }

        // Command to create a new meeting >>
        public void CreateMeeting()
        {
            Meeting meeting = new Meeting
            {
                Id = Guid.NewGuid(),                                        // Guid.Name - metodas sukuria siuo atveju individual names
                Description = GetInput("Enter Description: "),
                ResponsiblePerson = GetInput("Enter ResponsiblePerson: "),
                Category = GetCategory(),
                Type = GetType(),
                StartDate = GetDateInput("Enter StartDate: "),
                EndDate = GetDateInput("Enter EndDate: ")
            };

            while (meeting.EndDate < meeting.StartDate) //loop until start is earlier than the end
            {
                meeting.EndDate = GetDateInput("Enter EndDate that is later than StartDate: ");
            }

            // Enter meeting people names.
            List<string> names = new List<string>();
            while (true)
            {
                string name = GetInput("Enter name (leave blank if you are done with input): ");
                if (name != string.Empty)   //if (name != "")
                {
                    if (!names.Contains(name))
                        names.Add(name);
                    else
                        Console.WriteLine("Duplicate name is not allowed");
                }
                else
                    break;
            }

            if (!names.Contains(meeting.ResponsiblePerson))         //Check are name is not in names list
                names.Add(meeting.ResponsiblePerson);

            meeting.Name = names.ToArray();                 // convert lis t to array
            meetings = meetings.Append(meeting).ToArray();      // append - prideda
        }
        // Command to create a new meeting <<


        // Command to delete a meeting >>
        private void DeleteMeeting()
        {
            Console.WriteLine("In order to delete a meeting we need to identify you first");
            Meeting[] meetingList = FilterByResponsiblePerson(meetings, GetInput("Input responsible person filter: "));
            if (meetingList != null && meetingList.Length > 0)
            {
                if (TryGetMeeting(meetingList, SelectMeetingId(meetingList), out Meeting meeting))
                {
                    meetings = meetings.Where(val => val != meeting).ToArray();
                    Console.WriteLine("Meeting has been deleted");
                }
                else
                    Console.WriteLine(MeetingNotFoundText);
            }
            else
                Console.WriteLine(MeetingNotFoundText);
        }
        // Command to delete a meeting <<


        // Command to add a person to the meeting >>
        private void AddPersonToMeeting()
        {
            Meeting[] meetingList = FilterByStartDate(meetings, GetDateInput("Input start date filter "));
            if (meetingList != null && meetingList.Length > 0)
            {
                string name = GetInput("Enter name of the person to add to a meeting: ");
                if (FilterByAttendee(name, meetingList))
                    Console.WriteLine("WARNING: The person is already booked for a meeting during that time.");

                if (TryGetMeeting(meetingList, SelectMeetingId(meetingList), out Meeting meeting))
                {
                    if (meeting.Name.Contains(name.ToLower()))
                        Console.WriteLine("User is already in the meeting, skipping your request");
                    else
                    {
                        meeting.Name = meeting.Name.Append(name).ToArray();
                        meetings.SetValue(meeting, GetMeetingArrayId(meeting.Id, meetings)); // atnaujinta meeting patalpina vietoj orginalo i ta pacia pozicija/ta pati id
                        Console.WriteLine("User has been added to the meeting");
                    }
                }
                else
                    Console.WriteLine(MeetingNotFoundText);
            }
            else
                Console.WriteLine(MeetingNotFoundText);
        }
        // Command to add a person to the meeting <<


        // Command to remove a person from the meeting >>
        private void RemovePersonFromMeeting()
        {
            string name = GetInput("Input attendee name filter: ");
            Meeting[] meetingList = FilterByAttendee(meetings, name);
            if (meetingList != null && meetingList.Length > 0)
            {
                if (TryGetMeeting(meetingList, SelectMeetingId(meetingList), out Meeting meeting))
                {
                    if (meeting.ResponsiblePerson.ToLower() == name.ToLower())
                        Console.WriteLine("User is set as ResponsiblePerson he cannot be deleted!");
                    else
                    {
                        meeting.Name = meeting.Name.Where(val => val != name).ToArray();
                        meetings.SetValue(meeting, GetMeetingArrayId(meeting.Id, meetings));
                        Console.WriteLine("User has been deleted from the meeting");
                    }
                }
                else
                    Console.WriteLine(MeetingNotFoundText);
            }
            else
                Console.WriteLine(MeetingNotFoundText);
        }
        // Command to remove a person from the meeting <<


        // Command to list all the meetings >>
        public Meeting[] FilterMeetings()
        {
            string navigationValues = GetFilterNavigation();
            FilterNavigation option = (FilterNavigation)GetNumberInput(string.Format("Navigation {0}\n" + "Select action: ", navigationValues));

            Meeting[] meetingsLocal = null;
            switch (option)
            {
                case FilterNavigation.ByDescription:
                    meetingsLocal = FilterByDescription(meetings, GetInput("Specify meeting description: "));
                    break;
                case FilterNavigation.ByResponsiblePerson:
                    meetingsLocal = FilterByResponsiblePerson(meetings, GetInput("Specify responsible person: "));
                    break;
                case FilterNavigation.ByCategory:
                    meetingsLocal = FilterByCategory(meetings);
                    break;
                case FilterNavigation.ByType:
                    meetingsLocal = FilterByType(meetings);
                    break;
                case FilterNavigation.ByDate:
                    meetingsLocal = FilterByStartDate(meetings, GetDateInput("Specify start date: "));
                    meetingsLocal = FilterByEndDate(meetingsLocal, GetDateInput("Specify end date: "));
                    break;
                case FilterNavigation.ByNumberOfAttendees:
                    meetingsLocal = FilterByNumberOfAttendees(meetings, GetNumberInput("Specify amount of attendees: "));
                    break;
                default:
                    Console.WriteLine("Incorrect navigation type selected, returning to main menu.");
                    break;
            }

            if (meetingsLocal != null && meetingsLocal.Length > 0)
                ListMeetingIds(meetingsLocal);
            else
                Console.WriteLine(MeetingNotFoundText);
            return meetingsLocal;
        }
        // Command to list all the meetings <<

        private string GetInput(string prompt)    //call parameter
        {
            string output = String.Empty;
            Console.Write(prompt);
            try
            {
                output = GetAutomaticValues();
                if (output == null)
                    output = Console.ReadLine();
            }
            catch (Exception) { }
            if (output == null)
                output = String.Empty;
            return output;
        }

        private DateTime GetDateInput(string prompt)
        {
            while (true)
            {
                try
                {
                    return DateTime.Parse(GetInput(prompt));
                }
                catch (FormatException)
                {
                    Console.WriteLine("Wrong format used, try again");
                }
            }
        }

        private int GetNumberInput(string prompt)
        {
            while (true)
            {
                try
                {
                    return Convert.ToInt16(GetInput(prompt));
                }
                catch (FormatException)
                {
                    Console.WriteLine("\n\x4Wrong format used, try again: ");
                }
            }
        }

        private string GetAutomaticValues()
        {
            string returnText = null;
            if (AutomaticValues.Count > 0)
            {
                returnText = AutomaticValues.First();
                AutomaticValues.RemoveAt(0);
                Console.Write(string.Format("{0} ", returnText));
            }
            return returnText;
        }

        private Category GetCategory()
        {
            string CategoryIdentifiers = string.Empty;
            foreach (Category category in Enum.GetValues(typeof(Category)))
            {
                CategoryIdentifiers += string.Format("{0}-{1} ", (int)category, category);
            }
            string Prompt = string.Format("Select Category ({0}): ", CategoryIdentifiers);

            while (true)
            {
                Category cat = (Category)GetNumberInput(Prompt);
                if (Enum.IsDefined(typeof(Category), cat))
                    return cat;
                Console.WriteLine("Invalid Category was given");
            }
        }

        private Models.Type GetType()
        {
            string TypeIdentifiers = string.Empty;
            foreach (Models.Type type in Enum.GetValues(typeof(Models.Type)))
            {
                TypeIdentifiers += string.Format("{0}-{1} ", (int)type, type);
            }
            string Prompt = string.Format("Select Type ({0}): ", TypeIdentifiers);

            while (true)
            {
                Models.Type type = (Models.Type)GetNumberInput(Prompt);
                if (Enum.IsDefined(typeof(Models.Type), type))
                    return type;
                Console.WriteLine("Invalid Type was given");
            }
        }

        private int SelectMeetingId(Meeting[] meetingList)
        {
            ListMeetingIds(meetingList);
            return GetNumberInput("Select meeting to process: ");
        }

        private void ListMeetingIds(Meeting[] meetingList)
        {
            int i = 1;
            foreach (var meetingLocal in meetingList)
            {
                Console.WriteLine("{0}: {1}, Starts: {2}. Ends: {3}", i++, meetingLocal.Description, meetingLocal.StartDate, meetingLocal.EndDate);
            }
        }

        private Meeting[] FilterByCategory(Meeting[] meetings)
        {
            List<Meeting> meetingList = new List<Meeting>();
            Category filter = GetCategory();
            foreach (var meeting in meetings)
            {
                if (meeting.Category == filter)
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private Meeting[] FilterByType(Meeting[] meetings)
        {
            List<Meeting> meetingList = new List<Meeting>();
            Models.Type filter = GetType();
            foreach (var meeting in meetings)
            {
                if (meeting.Type == filter)
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private Meeting[] FilterByDescription(Meeting[] meetings, string filter)
        {
            List<Meeting> meetingList = new List<Meeting>();
            foreach (var meeting in meetings)
            {
                if (meeting.Description.ToLower().Contains(filter.ToLower()))
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private bool FilterByAttendee(string filter, Meeting[] meetings)
        {
            foreach (var meeting in meetings)
            {
                if (meeting.Name.Contains(filter.ToLower()))        //kartu vercia mazosiom
                    return true;
            }
            return false;
        }


        private Meeting[] FilterByAttendee(Meeting[] meetings, string filter)
        {
            List<Meeting> meetingList = new List<Meeting>();
            foreach (var meeting in meetings)
            {
                if (meeting.Name.Contains(filter.ToLower()))
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private Meeting[] FilterByResponsiblePerson(Meeting[] meetings, string filter)
        {
            List<Meeting> meetingList = new List<Meeting>();
            foreach (var meeting in meetings)
            {
                if (meeting.Name.Contains(filter.ToLower()))
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private Meeting[] FilterByStartDate(Meeting[] meetings, DateTime startFilter)
        {
            List<Meeting> meetingList = new List<Meeting>();
            foreach (var meeting in meetings)
            {
                if (meeting.StartDate == startFilter)
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private Meeting[] FilterByEndDate(Meeting[] meetings, DateTime endFilter)
        {
            List<Meeting> meetingList = new List<Meeting>();
            foreach (var meeting in meetings)
            {
                if (meeting.EndDate == endFilter)
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private Meeting[] FilterByNumberOfAttendees(Meeting[] meetings, int filter)
        {
            List<Meeting> meetingList = new List<Meeting>();
            foreach (var meeting in meetings)
            {
                if (meeting.Name.Length >= filter)
                    meetingList.Add(meeting);
            }
            return meetingList.ToArray();
        }


        private int GetMeetingArrayId(Guid Id, Meeting[] meetings) // suranda original meeting position sarase
        {
            int arrayIndex = 0;
            foreach (var meeting in meetings)
            {
                if (meeting.Id == Id)
                    return arrayIndex;
                arrayIndex++;
            }
            return -1;
        }


        private bool TryGetMeeting(Meeting[] meetingList, int id, out Meeting meeting)
        {
            try
            {
                meeting = meetingList[id - 1];
                return true;
            }
            catch (Exception) { }
            meeting = null;
            return false;
        }

        private static string GetMainNavigation()
        {
            string Identifiers = string.Empty;
            foreach (MainNavigation ident in Enum.GetValues(typeof(MainNavigation)))
            {
                Identifiers += string.Format("\n{0}: {1}", (int)ident, ident);
            }
            return Identifiers;
        }

        private static string GetFilterNavigation()
        {
            string Identifiers = string.Empty;
            foreach (FilterNavigation ident in Enum.GetValues(typeof(FilterNavigation)))  //grazina enum kaip array
            {
                Identifiers += string.Format("\n{0}: {1}", (int)ident, ident);
            }
            return Identifiers;
        }
    }
}
