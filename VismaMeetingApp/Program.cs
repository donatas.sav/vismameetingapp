﻿using Visma_meetings;

string dbPath = (args.Length > 0) ? args[0] : "db.json";
var App = new MeetingApp(dbPath);
App.Start();